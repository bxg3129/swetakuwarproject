package com.cerotid.first.assignment;

public class EmployeeTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Making an object
		
		
		//employee1 is an instance of Employee class.
		Employee employee1 = new Employee();
		employee1.name ="John";
		employee1.ssn = "123-45-6789";
		employee1.salary = 10000.00;
		
		Employee employee2 = new Employee();
		employee2.name ="Harry";
		employee2.ssn = "123-45-9876";
		employee2.salary = 30000.00;
		
		System.out.println("Print 1st Employee Information");
		employee1.printEmployeeInfo();
		employee1.printSocialSecurityAnName();
				
		System.out.println("Print 2nd Employee Information");
		employee2.printEmployeeInfo();
		employee2.printSocialSecurityAnName();
				
			
		

	}

}
