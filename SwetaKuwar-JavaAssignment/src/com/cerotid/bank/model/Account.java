package com.cerotid.bank.model;

public class Account {
	String accountType;

	// Access and Modifier
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	// Behaviors
	void printAccountInfo() {
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return "Account [accountType=" + accountType + "]";
	}
	

}
