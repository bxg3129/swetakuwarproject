package com.cerotid.bank.model;

import java.util.ArrayList;

public class Customer {
	String customerName;
	ArrayList<Account> accounts;
	String adddress;

	// Accessors and Modifier

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	public String getAdddress() {
		return adddress;
	}

	public void setAdddress(String adddress) {
		this.adddress = adddress;
	}
	 //Behaviors
	public void printCustomersAccounts() {
		//TODO print customers account logic
		System.out.println();
	}

	public void printCustomersDetails() {
	System.out.println(toString());
	}

	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", accounts=" + accounts + ", adddress=" + adddress + "]";
	}
	
}
