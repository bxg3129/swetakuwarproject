package com.cerotid.bank.model;

import java.util.ArrayList;

public class BankTester {

	public static void main(String[] args) {
		Bank bank = new Bank();
		// Bank inside (CustomerList and AccountList)
		// Customer1 all Info only.
		Account customer1Account1 = new Account();
		customer1Account1.setAccountType("Checking");

		Account customer1Account2 = new Account();
		customer1Account2.setAccountType("Saving");

		Account customer1Account3 = new Account();
		customer1Account3.setAccountType("Bussiness Checking");

		ArrayList<Account> customer1Accounts = new ArrayList<Account>();
		customer1Accounts.add(customer1Account1);
		customer1Accounts.add(customer1Account2);
		customer1Accounts.add(customer1Account3);

		Customer customer1 = new Customer();
		customer1.setAdddress("123 Ector dr, TX, 12345");
		customer1.setCustomerName("John Deo");
		customer1.setAccounts(customer1Accounts);
		// Customer2 info
		Customer customer2 = new Customer();
		Customer customer3 = new Customer();

		ArrayList<Customer> customerList = new ArrayList<Customer>();
		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);

		bank.setCustomers(customerList);

		bank.printBankName();
		bank.printBankDetails();
	}
}
