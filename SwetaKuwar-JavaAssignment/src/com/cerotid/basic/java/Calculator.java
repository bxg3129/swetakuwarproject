package com.cerotid.basic.java;

public class Calculator {
	// class level variables - hold state
	int result;

	public static void main(String[] args) {
		// instantiation
		// create calculator object
		Calculator myFirstObject = new Calculator();
		int sum = myFirstObject.sum(200, 100);
		System.out.println(sum);

		sum = myFirstObject.sum(10, 20, 30);
		System.out.println(sum);
		myFirstObject.multiply(2, 10);
		System.out.println(myFirstObject.result);

		Calculator mySecondObject = new Calculator();
		mySecondObject.sum(20, 30);
		System.out.println(sum);

	}

	// methods are behaviors
	// sum
	int sum(int a, int b) {
		return a + b;
	}

	int sum(int a, int b, int c) {
		return a + b + c;
	}

	void multiply(int a, int b) {
		// local variable
		// int random = 9;
		result = a * b;
	}

}
