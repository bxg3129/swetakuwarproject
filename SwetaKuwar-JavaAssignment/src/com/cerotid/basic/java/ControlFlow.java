package com.cerotid.basic.java;

public class ControlFlow {

	public static void main(String[] args) {
		int age = 68;
		
		if (age < 5) {
			System.out.println("Go to DayCare");
			
		}
		else if(age >= 5 && age <18){
			System.out.println("go to school");
		}
		else if (age >= 18 && age < 24) {
			System.out.println("go to collage");
		}
		else {
			System.out.println("go to work");
			
		}
	
	//If
	if ( age >=65) {
		System.out.println("You get socialsecurity benefits");	
	}
	//Expression
	//? Then
	//Else
	String seniority = (age >=65) ? "Senior" : "Not yet senior";
	
	//equivalent to above
	if(age >= 65) {
		seniority ="Senior";
	}
	else 
		seniority ="Not yet senior";
	
	
	
	//Same as if (if this do this ,or do this or do that)

	//Switch case and If (are Same).

	String sports = "Football";
	
	switch (sports) {
	case "Football":
		System.out.println("I love football");
	break;
	
	case "Baseball":
		System.out.println("I love baseball");
	break;
	
	default:
		System.out.println("I dont like sports");
	break;
	
	}

}}