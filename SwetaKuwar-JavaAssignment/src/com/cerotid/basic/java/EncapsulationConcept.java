package com.cerotid.basic.java;

//encapsulation is concept to protect the variable and give right number to access.
public class EncapsulationConcept {
	private String name;
	private String ssn;

	void printEmployeeInfo() {
		System.out.println("Name: " + name + " SSN : " + ssn);
	}
	// setter - Mutator
	// getter - Accessor

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		if (ssn.length() != 9) {
			this.ssn = null;
			return;
		}

	}

}
