package com.cerotid.basic.java;

public class ArrayConcept {

	public static void main(String[] args) {
		//Array concept (Like a bucket of container)
		
		int [] arrayInteger = new int [10];
		
		arrayInteger[0] = 100;
		arrayInteger[5] = 500;
		arrayInteger[8] = 200;
		arrayInteger[1] = 50;
		
	System.out.println("Value in Array position 1: " + arrayInteger[1]);
	System.out.println("Value in Array position 2: " + arrayInteger[2]);
	
	for (int i =0 ; i< 10; i++) {
	System.out.println("Value in Array position "+ i + ": " + arrayInteger[i]);
	}
	}
}
