package com.cerotid.basic.java;

import java.util.Scanner;

public class ScannerInputConcept {

	public static void main(String[] args) {
		int i = 1;
		do {

			System.out.println("Enter you name");

			Scanner input = new Scanner(System.in);

			String userName = input.nextLine();

			printUserInput(userName);

			int age = input.nextInt();

			printUserInput(age);

			i++;

		} while (i <= 5);

	}

	private static void printUserInput(Object userName) {
		System.out.println("You entered: " + userName);
	}

}
