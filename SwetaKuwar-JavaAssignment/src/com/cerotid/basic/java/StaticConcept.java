package com.cerotid.basic.java;

public class StaticConcept {
	// class level
	// both are member variable.
	static String name = "John";
	// class level. if one object is change other also change.
	// Static variable
	static int age = 31;
	// for instance variable( eg. int age = 20;)

	public static void main(String[] args) {
		StaticConcept sc1 = new StaticConcept();
		sc1.name = "DAvid";
		sc1.age = 25;

		printStaticConceptDetails("sc1:: ", sc1.name, sc1.age);

		StaticConcept sc2 = new StaticConcept();
		sc2.name = "Pie";

		printStaticConceptDetails("sc2:: ", sc2.name, sc2.age);
		// Due to static, name change after its change in Sc2.
		printStaticConceptDetails("sc1:: ", sc1.name, sc1.age);

		// In order to access static variable or method.
		// we don't have to instantiate
	}

	public static void printStaticConceptDetails(String ObjectName, String name, int age) {
		System.out.println("object " + ObjectName);
		System.out.println("Name " + name);
		System.out.println("age " + age);
	}
}
